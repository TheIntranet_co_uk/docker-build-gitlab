#!/usr/bin/env sh

set -eu

reset="\033[0m"
debug="\033[36m"    # cyan
error="\033[31m"    # red
info="\033[34m"     # blue
success="\033[32m"  # green
warning="\033[33m"  # yellow

print_debug(){
  if [ "${CI_COMMIT_REF_NAME:-`git rev-parse --abbrev-ref HEAD`}" != "master" ]; then
    printf "$debug> %s$reset\n" "$1"
  fi
}

print_error(){
    printf "$error> %s$reset\n" "$1"
}

print_info(){
    printf "$info> %s$reset\n" "$1"
}

print_success(){
    printf "$success> %s$reset\n" "$1"
}

print_warning(){
    printf "$warning> %s$reset\n" "$1"
}

if [ "${CI_COMMIT_REF_NAME:-`git rev-parse --abbrev-ref HEAD`}" != "master" ]; then
  print_debug "Enabling debug mode; ${CI_COMMIT_REF_NAME:-`git log -1 --oneline`}"
  set -x
fi
