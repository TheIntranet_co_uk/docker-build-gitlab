#!/usr/bin/env sh

source "$(dirname "$0")/_.sh"

set -eu

DOCKER_PATH=${1:-.}
REPO_PATH=${2:-""}
DOCKER_TAG=${3:-$CI_COMMIT_SHA}
EXTRA_ARGS=${4:-""}

CI_REGISTRY_FULLURL="${CI_REGISTRY_IMAGE}${REPO_PATH}"
DOCKER_CACHE_FROM=""

CI_COMMIT="${CI_COMMIT_BRANCH:-$CI_COMMIT_TAG}"


tag_push () {
  if [ "${CI_COMMIT_REF_NAME}" == "master" ]; then
    print_success "Tagging ${CI_REGISTRY_FULLURL}:${DOCKER_TAG} as 'master'"
    docker tag "${CI_REGISTRY_FULLURL}:${DOCKER_TAG}" "${CI_REGISTRY_FULLURL}:master"
  fi

  print_success "Tagging ${CI_REGISTRY_FULLURL}:${DOCKER_TAG} as 'latest'"
  docker tag "${CI_REGISTRY_FULLURL}:${DOCKER_TAG}" "${CI_REGISTRY_FULLURL}:latest"
  docker tag "${CI_REGISTRY_FULLURL}:${DOCKER_TAG}" "${CI_REGISTRY_FULLURL}:${CI_COMMIT}"

  docker push "${CI_REGISTRY_FULLURL}"
  exit 0
}

# Login to GitLab
docker login -u gitlab-ci-token -p "${CI_BUILD_TOKEN}" "${CI_REGISTRY}"

# Check if DOCKER_TAG exists in GitLab registry
if docker pull "${CI_REGISTRY_FULLURL}:${DOCKER_TAG}"; then
  print_error "Docker image ${CI_REGISTRY_FULLURL}:${DOCKER_TAG} already exists"
  tag_push
fi

# Pull branch/master/latest image (and also ignore if it doesn't exist)
if docker pull "${CI_REGISTRY_FULLURL}:${CI_COMMIT}"; then
  DOCKER_CACHE_FROM="--cache-from ${CI_REGISTRY_FULLURL}:${CI_COMMIT}"
  print_info "Docker cache will be used from ${CI_REGISTRY_FULLURL}:${CI_COMMIT}"
  if ! docker pull "${CI_REGISTRY_FULLURL}:latest"; then
    print_warning "${CI_REGISTRY_FULLURL}:latest doesn't exist"
  fi
else
  if docker pull "${CI_REGISTRY_FULLURL}:latest"; then
    DOCKER_CACHE_FROM="--cache-from ${CI_REGISTRY_FULLURL}:latest"
    print_debug "Docker cache will be used from ${CI_REGISTRY_FULLURL}:latest"
  fi
  if docker pull "${CI_REGISTRY_FULLURL}:master"; then
    DOCKER_CACHE_FROM="--cache-from ${CI_REGISTRY_FULLURL}:master"
    print_debug "Docker cache will be used from ${CI_REGISTRY_FULLURL}:master"
  fi
fi

if -z "${DOCKER_CACHE_FROM}"; then
  print_warning "No docker cache will be used"
fi

BUILD_CMD="docker build ${DOCKER_CACHE_FROM} -t ${CI_REGISTRY_FULLURL}:${DOCKER_TAG} ${EXTRA_ARGS} ${DOCKER_PATH}"

print_info "Building Dockerfile: \033[1;32m${BUILD_CMD}"
$(echo "${BUILD_CMD}")

tag_push
